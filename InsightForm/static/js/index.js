function validateEmail() {
    var myemail = document.myform.myemail.value;
       

    if (myemail === ""||myemail==null||myemail != "/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/") {
        document.getElementById("div2").innerHTML="Enter your email";
        // document.getElementById("div1").style.color="Red";
        return false;
    }
    else{
        document.getElementById("div2").innerHTML="";
        return true;
    }
  } 

  function validateName(){
      var fullname = document.myform.myname.value;
      console.log(fullname);
      if (fullname === ""||fullname==null|| fullname != /^([a-zA-Z]+)$/) {
        document.getElementById("div1").innerHTML="Name must be filled";
        // document.getElementById("div1").style.color="Red";
        return false;
        }
        else{
            document.getElementById("div1").innerHTML="";
            return true;
        }
  }
  function validateNumber(){
      var tel = document.myform.tel.value;
      if(tel === ""||tel == null || tell != "/^((\+*)[9]77-*)*[9]{1}[0-9]{9}$/"){
          document.getElementById("div3").innerHTML="Please enter your contact number";
          return false;
      }
      else{
          document.getElementById("div3").innerHTML=""
          return true;
      }
  }

  function validateMotivation(){
      var textarea = document.myform.textarea.value;
      if(textarea === ""||textarea == null){
        document.getElementById("div4").innerHTML="Please share your motivation to join the program";
        return false;
        }
        else{
            document.getElementById("div4").innerHTML=""
            return true;
        }
  }
  function validateFile(){
      var cvfile = document.myform.cvfile.value;
      var ext = cvfile.substring(cvfile.lastIndexOf('.')+1);
      if(ext == "pdf"){
          console.log("Ok");
          return true;
      }
      else{
        document.getElementById("div5").innerHTML="Please upload your CV";
        return false;
      }
  }

  function validateForm(){
      validateName();
      validateEmail();
      validateNumber();
      validateFile();
      validateMotivation();
      return false;
  }