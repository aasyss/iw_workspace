from django.apps import AppConfig


class ForminsightConfig(AppConfig):
    name = 'forminsight'
