from django.db import models


# Create your models here.


class Post(models.Model):
    fullname = models.CharField(max_length=25, unique=True, blank=False)
    email = models.CharField(max_length=30, blank=False)
    tel = models.CharField(max_length=20, blank=False)
    cv = models.FileField(blank=False)
    # defaultcheckbox1 = models.BooleanField("October")
    # defaultcheckbox2 = models.BooleanField("January", default=True)

    frmmotivation = models.TextField(default='whats your Motivation?')
