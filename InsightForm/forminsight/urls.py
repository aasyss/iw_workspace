from sys import path

from forminsight import views

urlpatterns = [
    path('', views.renderform(), name='index'),
]