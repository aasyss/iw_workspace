from django.contrib import admin

# Register your models here.
from forminsight.models import Post

admin.site.register(Post)
