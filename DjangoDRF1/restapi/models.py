from django.db import models

# Create your models here.

class MyModel(models.Model):
    username = models.CharField(max_length=40)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.CharField(max_length=50)

    def __str__(self):
        return self.username

class PhoneModel(models.Model):
    user_id = models.ForeignKey(MyModel, on_delete=models.CASCADE, related_name='contact_information')

    phone = models.CharField(max_length=20)

    def __str__(self):
        return self.phone