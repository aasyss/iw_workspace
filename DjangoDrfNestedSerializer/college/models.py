from django.db import models

# Create your models here.
class Colleges(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=50)


class Departments(models.Model):
    college = models.ForeignKey(Colleges,on_delete=models.CASCADE, related_name='department_info',blank=True,null=True)
    department_name = models.CharField(max_length=50)
    hod = models.CharField(max_length=50)
    students = models.IntegerField()