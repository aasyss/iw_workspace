from django.shortcuts import render

# Create your views here.

from django.contrib.auth.models import User
from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from college.models import Colleges, Departments
from college.serializers import CollegeSerializerWrite, CollegeSerializerRead, DepartmentSerializerWrite, \
    DepartmentSerializerRead

class CollegeViewSets(viewsets.ModelViewSet):
    queryset = Colleges.objects.all()
    serializer_class =  CollegeSerializerRead

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return CollegeSerializerRead
        else:
            return CollegeSerializerWrite

class DepartmentViewSets(viewsets.ModelViewSet):
    queryset = Departments.objects.all()
    serializer_class = DepartmentSerializerRead

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return DepartmentSerializerRead
        else:
            return DepartmentSerializerWrite

    def get_serializer_context(self):
        if self.request.method == "POST":
            college_data = self.request.data.pop('college')
            college = {"college": college_data}
            return college

        # context = super(DepartmentViewSets,self).get_serializer_context()
        # action = self.action
        #
        # if (action == 'create'):
        #     context['fields'] = ('id','name','address','department_name','hod','students')
        # return  context
        # return {'request':self.request}
