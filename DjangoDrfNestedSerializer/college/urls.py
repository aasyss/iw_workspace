from django.urls import path, include
from rest_framework import routers

from college.views import CollegeViewSets, DepartmentViewSets

router = routers.DefaultRouter()
router.register(r'colleges', CollegeViewSets,basename='Colleges')
router.register(r'departments',DepartmentViewSets,basename='Departments')

urlpatterns = [
    path('', include(router.urls)),

]