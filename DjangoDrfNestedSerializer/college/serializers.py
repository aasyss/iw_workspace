from rest_framework import serializers

from college.models import Colleges, Departments


class CollegeSerializerWrite(serializers.ModelSerializer):

    class Meta:
        model = Colleges
        fields = '__all__'

class CollegeSerializerRead(serializers.ModelSerializer):

    class Meta:
        model = Colleges
        fields = ['name','address','department_info']
        depth = 1

    def get_name(self, user):
        return "Mr./Mrs."+user.first_name + " " + user.last_name

class DepartmentSerializerWrite(serializers.ModelSerializer):
    # user = UserSerializerWrite()
    class Meta:
        model = Departments
        fields = '__all__'
        depth = 1

    def create(self, validated_data):
        college = self.context["college"]
        college = Colleges.objects.create(**college)
        department = Departments.objects.create(college=college,**validated_data)
        return department

class DepartmentSerializerRead(serializers.ModelSerializer):

    class Meta:
        model = Departments
        fields = '__all__'
        depth = 1

