from django.contrib import admin

# Register your models here.
from college.models import Departments, Colleges

admin.site.register(Colleges)
admin.site.register(Departments)