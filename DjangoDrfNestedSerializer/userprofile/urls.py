from django.urls import path, include
from rest_framework import routers

from userprofile.views import UserViewSets, ProfileViewSets

router = routers.DefaultRouter()
router.register(r'users', UserViewSets,basename='Users')
router.register(r'profile',ProfileViewSets,basename='Contact')

urlpatterns = [
    path('', include(router.urls)),

]