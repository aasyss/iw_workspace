from django.contrib.auth.models import User
from rest_framework import serializers

from userprofile.models import UserProfile

class UserSerializerWrite(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id','username','first_name','last_name','email']

class UserSerializerRead(serializers.ModelSerializer):
    name = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = ['id','username','name','first_name','last_name','email','user_details']
        depth = 1

    def get_name(self, user):
        return "Mr./Mrs."+user.first_name + " " + user.last_name


class ProfileSerializerWrite(serializers.ModelSerializer):
    user = UserSerializerWrite()
    class Meta:
        model = UserProfile
        # fields = '__all__'
        fields = ['phone','address','user']

    def create(self, validated_data):
        user_data=validated_data.pop('user')
        user = User.objects.create(**user_data)
        profile = UserProfile.objects.create(user=user,**validated_data)
        return profile

class ProfileSerializerRead(serializers.ModelSerializer):
    user = UserSerializerRead(many=False, read_only=True)

    class Meta:
        model = UserProfile
        fields = '__all__'



