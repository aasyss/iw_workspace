from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE,related_name="user_details")
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=20)