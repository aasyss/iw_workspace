from django.contrib.auth.models import User
from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from userprofile.models import UserProfile
from userprofile.serializer import UserSerializerWrite, UserSerializerRead, ProfileSerializerWrite, \
    ProfileSerializerRead


class UserViewSets(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class =  UserSerializerWrite

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return UserSerializerRead
        else:
            return UserSerializerRead

class ProfileViewSets(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = ProfileSerializerRead

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ProfileSerializerRead
        else:
            return ProfileSerializerWrite

