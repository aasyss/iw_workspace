class Calculator:
    # def __init__(self):
    #     pass

    # function to add two numbers
    def add(self, a, b):
        return a + b

    # function to subtract two numbers
    def subtract(self, a, b):
        return a - b

    # function to multiply two numbers
    def multiply(self, a, b):
        return a * b

    # function to multiply two numbers
    def divide(self, a, b):
        if b == 0:
            raise ZeroDivisionError("Cannot be divided by zero")
        else:
            return a / b


x = int(input("Enter first number: "))
y = int(input("Enter second number: "))

ob = Calculator()

print(ob.add(x, y))
print(ob.subtract(x, y))
print(ob.multiply(x, y))
print(ob.divide(x, y))
