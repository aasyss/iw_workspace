# from django.contrib.auth.models import User
# from django.db import models

# Create your models here.

# class UserModel(User):
#     phone = models.CharField(max_length=20)
#
#     def __str__(self):
#         return self.username
from django.db import models


class ProfileModel(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)

    def __str__(self):
        return self.name