from django.contrib.auth.models import User
from django.utils.deprecation import MiddlewareMixin
from rest_framework.authtoken.models import Token


class TokenRequiredMiddleware(MiddlewareMixin):
    def process_request(self,request):
        users = User.objects.all()
        for user in users:
            token, created = Token.objects.get_or_create(user=user)