from sys import path

from facebookhome import views

urlpatterns = [
    path('', views.index, name='index'),
]