$(document).ready(function () {
    $.ajax({

        type: "get",
        url: 'http://dummy.restapiexample.com/api/v1/employees',
        success: function (response) {
            console.log(response);
            var jsonObject = JSON.parse(response);

            var t = $('#example').DataTable({
                data: jsonObject,
                columns: [
                    { 'data': 'id' },
                    { 'data': 'employee_name' },
                    { 'data': 'employee_salary' },
                    { 'data': 'employee_age' },
                    { 'data': 'profile_image' }
                ],
            });

        }
    })
});