from django.contrib.auth.models import User
from django.db import models


GROUP_CHOICES = (
    ('admin','ADMIN'),
    ('staff','staff')
)

class MyUserModel(User):
    phone = models.CharField(max_length=15)
    def __str__(self):
        return super().username