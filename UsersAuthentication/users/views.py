from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.models import Group
from django.shortcuts import render, redirect

# Create your views here.
from users.middleware.backends import  CustomAuthenticate

from users.models import MyUserModel

def renderlogin(request):

    context = ''
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')

        user = CustomAuthenticate.customauthenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            if not request.user.is_authenticated:
                messages.warning(request, 'Please enter username and password to access home -page')
                return redirect(settings.LOGIN_URL)
            else:
                messages.info(request, 'Successfully logged in.')
                return redirect(settings.LOGIN_REDIRECT_URL)
        else:
            messages.error(request, 'Please enter username and password to access home -page')
            return redirect(settings.LOGIN_URL)
    return render(request, 'login.html', {'context': context})

def renderhome(request):
    return render(request,'home.html')

def rendersignup(request):
    if request.method == 'POST':
        data = MyUserModel(username=request.POST.get('username'), phone = request.POST.get('phone'))
        data.set_password(request.POST.get('password2'))
        data.save()
        my_group = Group.objects.get(name=request.POST.get('groups'))
        data.groups.add(my_group)
        my_group.save()

        return render(request,'login.html')
    return render(request,'signup.html')