from django.urls import path

from users import views

urlpatterns = [
    path('',views.renderlogin,name='login'),
    path('home/',views.renderhome,name='home'),
    path('signup/',views.rendersignup,name="signup")
]
