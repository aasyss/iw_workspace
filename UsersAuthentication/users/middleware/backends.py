from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from django.utils.deprecation import MiddlewareMixin


class CustomAuthenticate(MiddlewareMixin):
    def customauthenticate(self, username=None, password=None, ):
        User = get_user_model()
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return None
        else:
            if getattr(user, 'is_active', False) and user.check_password(password):
                return user
        return None

    def get_user(self, user_id):
        User = get_user_model()
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class LoginRequiredMiddleware(MiddlewareMixin):

    def process_request(self, request):

        if request.path == '/home/':
            if not request.user.is_authenticated:
                messages.error(request, 'Please enter username and password to access home -page')
                return redirect(settings.LOGIN_URL)
