import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserComponent} from "./user/user.component";
import {DetailUserComponent} from "./detail-user/detail-user.component";
import {AddUserComponent} from "./add-user/add-user.component";
import {EditUserComponent} from "./edit-user/edit-user.component";
import {LoginComponent} from "./login/login.component";
import {AuthGuard} from "./auth.guard";
import {RegisterComponent} from "./register/register.component";

const routes: Routes = [
  {
    path: 'users',
    component: UserComponent,
    canActivate:[AuthGuard],
    data: { title: 'User List' }
  },
  {
    path: 'users/users-details/:id',
    component: DetailUserComponent,
    canActivate:[AuthGuard],
    data: { title: 'User Details' }
  },
  {
    path: 'users/users-add',
    component: AddUserComponent,
    canActivate:[AuthGuard],
    data: { title: 'User Add' }
  },
  {
    path: 'users/users-edit/:id',
    component: EditUserComponent,
    canActivate:[AuthGuard],
    data: { title: 'User Edit' }
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  { path: '',
    redirectTo: '/users',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
