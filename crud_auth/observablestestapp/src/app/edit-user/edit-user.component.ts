import { Component, OnInit } from '@angular/core';
import {UserService} from "../user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {Users} from "../users";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  // private edit_user_form: FormGroup;
  // user$: Observable<any>;


  userData: any;
  private edit_user_form: FormGroup;

  constructor(public usrService: UserService, private form_builder: FormBuilder, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.usrService.getUserDetails(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);

      this.userData = data;

      //   this.edit_user_form = this.form_builder.group({
      //   username: '',
      //   email: '',
      //   first_name: '',
      //   last_name: ''
      //
      // });
      this.edit_user_form.controls['username'].setValue(this.userData['username']);
      this.edit_user_form.controls['email'].setValue(this.userData['email']);
      this.edit_user_form.controls['first_name'].setValue(this.userData['first_name']);
      this.edit_user_form.controls['last_name'].setValue(this.userData['last_name']);
    });

    this.edit_user_form = new FormGroup({
      username: new FormControl(),
      email: new FormControl(),
      first_name: new FormControl(),
      last_name: new FormControl(),
    });
  }

  onSubmit() {
    this.usrService.putUser(this.route.snapshot.params['id'],this.edit_user_form.value).subscribe((result)=>{
      // @ts-ignore
      this.router.navigateByUrl(['users/']);
    });
  }
}

  // public putUser(){
  //   this.usrService.putUser(this.route.snapshot.params['id'], this.userData).subscribe((result)=>{
  //
  //   })
  // }



