import { Injectable } from '@angular/core';
import {User} from "./user";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../environments/environment";
import {map} from "rxjs/operators";
import {BehaviorSubject, Observable} from "rxjs";
import {state} from "@angular/animations";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':'application/json'
  })
};

  constructor(private http: HttpClient , private router: Router) { }

  public login(userInfo):Observable<any>{
   return this.http.post<any>(`${environment.endpoint}rest-auth/login/`,userInfo,this.httpOptions);
  }

  public  register(newUserInfo):Observable<any>{
    return this.http.post<any>(`${environment.endpoint}rest-auth/registration/`,newUserInfo,this.httpOptions);
  }

  isLoggedIn() {
    // @ts-ignore
    // this.router.navigateByUrl(['/login']);
    return localStorage.getItem('ACCESS_TOKEN')!==null;
  }

  // getToken() {
  //   return localStorage.getItem('ACCESS_TOKEN');
  // }
}
