import { Component, OnInit } from '@angular/core';
import {UserService} from "../user.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import {Users} from "../users";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  user$: Observable<Users[]>;
  private user_form: FormGroup;

  constructor(public usrService: UserService, public form_builder: FormBuilder,private router: Router) { }

  ngOnInit() {
    this.user_form = this.form_builder.group({
      username: '',
      email: '',
      first_name: '',
      last_name: ''
    });
    this.user_form.controls["username"].setValidators([Validators.required]);
    this.user_form.controls["email"].setValidators([Validators.required]);
    this.user_form.controls["first_name"].setValidators([Validators.required]);
    this.user_form.controls["last_name"].setValidators([Validators.required]);

  }

  onSubmit() {
    this.usrService.postUser(this.user_form.value)
      .subscribe(
        (response) => {
          console.log(response);
          // @ts-ignore
          this.router.navigateByUrl(['users/']);
        }
      )
  }
}
