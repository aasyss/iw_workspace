import { Component, OnInit } from '@angular/core';
import {UserService} from "../user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {Users} from "../users";

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss']
})
export class DetailUserComponent implements OnInit {

  user : any;
  // user: {};
  constructor(public usrService: UserService, private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
    this.usrService.getUserDetails(this.route.snapshot.params['id']).subscribe((data: {}) =>{

      this.user = data;
    });
  }

}
