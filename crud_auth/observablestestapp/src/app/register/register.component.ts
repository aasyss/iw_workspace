import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  isSubmitted = false;
  loading =false;

  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm  =  this.formBuilder.group({
        username: ['', Validators.required],
        email: ['', Validators.required],
        // password:['',Validators.required],
        password1: ['', Validators.required],
        password2: ['',Validators.required]
    });
  }

    get formControls() { return this.registerForm.controls; }


  register() {
    this.isSubmitted = true;
    if(this.registerForm.invalid){
      return;
    }
    this.loading =true;
    this.authService.register(this.registerForm.value)
      .subscribe(
        (response) => {
          console.log(response);
          // localStorage.setItem('ACCESS_TOKEN',response['key']);
          this.router.navigate(['login']);
        }
      );
  }
}
