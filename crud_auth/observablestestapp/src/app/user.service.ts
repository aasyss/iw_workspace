import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Users} from "./users";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService{

  httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    // Authorization: ACCESS_TOKEN
  })
};
  // LOCAL_URL = 'http://localhost:8000/';

  constructor(private http: HttpClient) { }

  // intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  //   console.log('here');
  //   console.log(JSON.stringify(req));
  //   req = req.clone({
  //     setHeaders: {
  //       Authorization: `Token ${localStorage.getItem('ACCESS_TOKEN')}`
  //     }
  //   });
  //   return next.handle(req) ;
  // }

  public getUsers(): Observable<Users[]>
  {
    return this.http.get<Users[]>(`${environment.endpoint}users/`);
  }

  public postUser(user): Observable<any>{
    console.log(user);
    return this.http.post<any>(`${environment.endpoint}users/`, JSON.stringify(user), this.httpOptions)
  }

  public putUser(id,user): Observable<any>{
    // console.log(user);
    return this.http.put(`${environment.endpoint}users/`+id+'/', JSON.stringify(user),this.httpOptions);
  }

  // public editUser(id,user): Observable<any>{
  //   return this.http.get<any>(`${environment.endpoint}user/`+id).patchValue(id);
  // }

  public getUserDetails(id): Observable<any>{
    return this.http.get(`${environment.endpoint}users/`+id)
  }

  deleteUser(id): Observable<any>{
    return this.http.delete<any>(`${environment.endpoint}users/`+id, this.httpOptions)
  }
  //


}
