import {Injectable, Injector} from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from "@angular/common/http";
import {Observable} from "rxjs";
import {AuthService} from "./auth.service";
import {environment} from "../environments/environment";

@Injectable()
export class MyInterceptor implements HttpInterceptor{
  // auth: AuthService;
  constructor(){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // const lo = /login/;
    // const re = /register/;
    // if(req.url.search(lo)=== -1){
    //   req = req.clone({
    //   setHeaders: {
    //     Authorization: `Token ${localStorage.getItem('ACCESS_TOKEN')}`
    //   }
    // });
    // }
    if(req.url.match(`${environment.endpoint}users/`)){
      req = req.clone({
      setHeaders: {
        Authorization: `Token ${localStorage.getItem('ACCESS_TOKEN')}`
      }
    });
    }
    return next.handle(req) ;
  }

}
