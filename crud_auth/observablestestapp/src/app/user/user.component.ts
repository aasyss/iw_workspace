import { Component, OnInit } from '@angular/core';
import {Users} from "../users";
import {UserService} from "../user.service";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  user$ = new Array<Users>();

  constructor(public usrService:UserService) {}

  ngOnInit() {
     this.getUsers();
  }
  public getUsers(){
    this.usrService.getUsers().subscribe(response => {
      this.user$ = response.map(item => {
          return new Users(
            item.id,
            item.username,
            item.email,
            item.first_name,
            item.last_name,
            // item.phone,
          );
        });
    });
  }
  public deleteUsers(id){
    this.usrService.deleteUser(id).subscribe((response)=>{
      this.getUsers()
    })
  }

}
