from rest_framework import serializers

from userprofileapp.models import MyModel, PhoneModel


class UserSerializer(serializers.ModelSerializer):
    # name = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = MyModel
        fields = ['id','username','first_name','last_name','email']
        # extra_kwargs = {
        #     'first_name': {'write_only': True},
        #     'last_name': {'write_only': True},
        # }
        depth = 1

    # def get_name(self, user):
    #     return user.first_name + " " + user.last_name

    def create(self, validated_data):
        return MyModel.objects.create(**validated_data)


# class PhoneSerializer(serializers.ModelSerializer):
#     phone = serializers.CharField()
#     class Meta:
#         model = PhoneModel
#         fields  = ['id','phone','user_id']

# class PhoneSerializerGet(serializers.ModelSerializer):
#     # phone = serializers.CharField()
#
#     user_info = UserSerializer(many=False,read_only=True)
#     class Meta:
#         model = PhoneModel
#         # fields = ['user_info']
#         fields = '__all__'
#         depth = 1
