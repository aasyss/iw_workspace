from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from userprofileapp.models import MyModel
from userprofileapp.serializer import UserSerializer


class UserViewSets(viewsets.ModelViewSet):

    queryset = MyModel.objects.all()
    serializer_class = UserSerializer

# class ContactViewSets(viewsets.ModelViewSet):
#
#     queryset = PhoneModel.objects.all()
#     serializer_class = PhoneSerializer
#
#     def get_serializer_class(self):
#         if self.request.method == 'GET':
#             return PhoneSerializerGet
#         else:
#             return PhoneSerializer
