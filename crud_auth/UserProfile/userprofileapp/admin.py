from django.contrib import admin

# Register your models here.
from userprofileapp.models import MyModel, PhoneModel

admin.site.register(MyModel)
admin.site.register(PhoneModel)
