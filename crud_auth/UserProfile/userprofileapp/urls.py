from django.urls import path, include
from rest_framework import routers

from userprofileapp.views import UserViewSets

router = routers.DefaultRouter()
router.register(r'users', UserViewSets,basename='Users')
# router.register(r'contact',ContactViewSets,basename='Contact')

urlpatterns = [
    path('', include(router.urls)),
    path('rest-auth/',include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
]
