from sys import path

from myapp import views

urlpatterns = [
    path('', views.home_page(), name='homepage'),
    path('update/<int:id>', views.book_edit(), name="edit"),
    # path('update/<int:id>',views.books_update(),name = "update"),
    path('', views.books_list(), name='list'),
    path('book-create/', views.books_create(), name='index'),

]
