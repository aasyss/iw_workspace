from django.forms import forms

from myapp.models import Books


class BookForm(forms.ModelForm):
    class Meta:
        model = Books
        fields = '__all__'
