import random

from django.core.management import BaseCommand
from django_seed import Seed

from myapp.models import Books

seeder = Seed.seeder()

seeder.add_entity(Books, 10, {
    'book_name': lambda x: seeder.faker.first_name(),
    'author_name': lambda x: seeder.faker.name(),
    'rate': lambda x: seeder.faker.pyint(min_value=1000, max_value=3000, step=1),
    'quantity': lambda x: seeder.faker.pyint(min_value=1, max_value=10, step=1)
})


class Command(BaseCommand):
    help = "Python Script to Populate data"

    def handle(self, *args, **options):
        seeder.execute()
