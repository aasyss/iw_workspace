from django.shortcuts import render, redirect, render_to_response
from django.contrib import messages

# Create your views here.
from myapp.models import Books


def home_page(request):
    return render(request, 'homepage.html', context=None)


def books_create(request):
    if request.method == 'POST':
        books = Books.objects.create(
            book_name=request.POST.get('book_name'),
            author_name=request.POST.get('author_name'),
            rate=request.POST.get('rate'),
            quantity=request.POST.get('quantity'),
        )
        books.save()
        context = {
            "value": "Successfully submitted data",
        }
        return render(request, 'index.html', context=context)
    return render(request, 'index.html')


def books_list(request):
    books = Books.objects.all()
    context = {
        "books": books
    }
    # book = Books.objects.all()

    return render(request, 'list.html', context=context)


def book_edit(request, pk):
    if request.method == 'POST':
        print("aas")
        book_name = request.POST.get('book_name')
        author_name = request.POST.get('author_name')
        rate = request.POST.get('rate')
        quantity = request.POST.get('quantity')
        Books.objects.filter(id=pk).update(book_name=book_name, author_name=author_name, rate=rate, quantity=quantity)
        print(book_name)

        return redirect('/book-list')
    book = Books.objects.get(id=pk)
    return render(request, 'update.html',
                  {'book_name': book.book_name, 'author_name': book.author_name, 'rate': book.rate,
                   'quantity': book.quantity})


def book_delete(request, pk):
    book = Books.objects.get(id=pk)
    book.delete()
    messages.success(request,'{0} Deleted'.format(book))
    return redirect('list')
