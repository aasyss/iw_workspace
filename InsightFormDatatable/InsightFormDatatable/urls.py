"""InsightFormDatatable URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from myapp.views import HomepageView, ApplicantListView, ApplicantDetailView, ApplicantCreateView, ApplicantUpdateView, \
    ApplicantDeleteView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', HomepageView.as_view(), name='home'),
    path('list/', ApplicantListView.as_view(), name='list'),
    path('details/<int:pk>/', ApplicantDetailView.as_view(), name='details'),
    path('create/', ApplicantCreateView.as_view(), name='applicant-create'),
    path('list/<int:pk>/', ApplicantUpdateView.as_view(), name='applicant-update'),
    # path('list/<int:id>/', ApplicantUpdateView.as_view(), name='applicant-update'),
    path('delete/<int:pk>/', ApplicantDeleteView.as_view(), name='delete'),
]
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
