from django import forms

from myapp.models import Applicant


class ApplicantCreateForm(forms.ModelForm):
    class Meta:
        model = Applicant
        fields = '__all__'
