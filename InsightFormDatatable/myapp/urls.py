from sys import path

from django.conf import settings
# from django.template.context_processors import static
from django.conf.urls.static import static

from myapp.views import ApplicantListView, ApplicantCreateView, ApplicantUpdateView, ApplicantDeleteView, HomepageView, \
    ApplicantDetailView

urlpatterns = [
    path('', HomepageView.as_view(), name='home'),
    path('list/', ApplicantListView.as_view(), name='list'),
    path('details/<int:pk>/', ApplicantDetailView.as_view(),name='details'),
    path('create/', ApplicantCreateView.as_view(), name='applicant-create'),
    path('list/<int:id>/', ApplicantUpdateView.as_view(), name='applicant-update'),
    path('list/<int:id>/', ApplicantUpdateView.as_view(), name='applicant-update'),
    path('delete/<int:pk>/delete/', ApplicantDeleteView.as_view(), name='delete'),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
