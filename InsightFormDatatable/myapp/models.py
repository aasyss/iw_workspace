from django.db import models

# Create your models here.
# from django.forms import ModelForm, forms
# from django.urls import reverse
from django.urls import reverse


class Applicant(models.Model):
    fullname = models.CharField(max_length=25, unique=True, blank=False)
    email = models.EmailField(max_length=30, blank=False)
    tel = models.IntegerField(blank=False)
    cv = models.FileField(upload_to="documents",null=True,blank=True)
    batch = models.CharField(max_length=25, default="First Batch")
    motivation = models.TextField(max_length=200, default='whats your Motivation?')

    def __str__(self):
        return self.fullname

    def get_absolute_url(self):
        return reverse('list')
