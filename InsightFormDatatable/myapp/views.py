from django.core.files.storage import FileSystemStorage
from django.http import request
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DetailView, TemplateView
from django.views.generic.edit import DeleteView

from myapp.forms import ApplicantCreateForm
from myapp.models import Applicant


class HomepageView(TemplateView):
    template_name = 'myapp/applicant_home.html'


class ApplicantListView(ListView):
    model = Applicant


class ApplicantDetailView(DetailView):
    template_name = 'myapp/applicant_details.html'
    model = Applicant


class ApplicantCreateView(CreateView):
    model = Applicant
    fields = ['fullname', 'email', 'tel', 'cv', 'batch', 'motivation']
    # template_name = 'myapp/applicant_form.html'
    success_url = reverse_lazy('applicant-create')


class ApplicantUpdateView(UpdateView):
    # template_name = 'myapp/applicant_form1.html'
    model = Applicant
    fields = '__all__'
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)


class ApplicantDeleteView(DeleteView):
    template_name = 'myapp/applicant_delete.html'
    model = Applicant
    success_url = reverse_lazy('list')
