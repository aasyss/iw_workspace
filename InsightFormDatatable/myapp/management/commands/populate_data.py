import random

from django.core.management import BaseCommand
from django_seed import Seed

from myapp.models import Applicant

seeder = Seed.seeder()

seeder.add_entity(Applicant, 10, {
    'fullname': lambda x: seeder.faker.name(),
    'email': lambda x: seeder.faker.email(),
    'tel': lambda x: seeder.faker.pyint(min_value=9812549637, max_value=9899999999, step=1),
    'cv': lambda x : seeder.faker.file_name(category=None, extension='pdf'),
    'frmmotivation': lambda x: seeder.faker.text(max_nb_chars = 70)
})


class Command(BaseCommand):
    help = "Python Script to Populate data"

    def handle(self, *args, **options):
        seeder.execute()
