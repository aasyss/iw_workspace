$(document).ready(function () {
    console.log("hello world");

    // validate on blur event

    $("#fullname").blur(function () {
        check_name();
    });

    $("#email").blur(function () {
        check_email();
    });

    $("#tel").blur(function () {
        check_phone();
    });

    $("#frmmotivation").blur(function () {
        check_motivation();
    });

    function check_name() {
        var fullname = $('#fullname').val();
        var name_regex = /^([a-zA-Z]+)$/;

        if (!fullname.match(name_regex)|| fullname.length == 0) {
            $("#div1").html("* For your name please use alphabets only *"); // This Segment Displays The Validation Rule For Name
            $("#div1").show();

            // return true;
        }
        else {
            $("#div1").hide();

            // return false;
        }
    }

    function check_email() {
        var email = $('#email').val();
        var email_regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;


        if (!email.match(email_regex) || email.length == 0) {
            $('#div2').text("* Please enter a valid email address *"); // This Segment Displays The Validation Rule For Email
            // $("#email").focus();
            $("#div2").show();
            // return false;
        }
        else {
            $("#div2").hide();
            // return true;
        }
    }

    function check_phone() {
        var tel = $('#tel').val();
        var phone_regex = /^((\+*)[9]77-*)*[9]{1}[0-9]{9}$/;


        if (!tel.match(phone_regex) || tel.length == 0) {   // This Segment Displays The Validation Rule For Phone
            $("#div3").text("* This field should not be empty *");
            // $("#tel").focus();
            $("div3").show();
            // return false;
        }

        else {
            $("#div3").hide();
            // return true;
        }
    }

    function check_motivation() {
        var frmmotivation = $('#frmmotivation').val();

        if (frmmotivation.length == 0) {
            $('#div4').html("* Please share your motivation for joining this course *"); // This Segment Displays The Validation Rule For Motivation
            // $("#frmmotivation").focus();
            $("div4").show();
            return false;
        }
        else {
            $("#div4").hide();
            return true;
        }
    }

// validate after clicking submit button
    $("#sbtn").click(function () {
        check_name();
        check_email();
        check_phone();
        check_motivation();
    });
});